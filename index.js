const express = require("express");
//Mongoose is a package that allows creation of Schemas to model our data structures
//Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

//MongoDB connection
//connect to the database by passing in our connection string, remember to replace the password and databse names with actual values
//{newUrlParser:true} allows us to avoid current and future errors while connecting to MongoDB

/*
	Syntax
		mongoose.connect("<MongoDB connect string>", {useNewUrlParser : true})
*/
//Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://admin123:admin123@project0.n6empch.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewURLParser: true,
		useUnifiedTopology: true
	}
);

//Set notifications for connecting success or failure
//allows us to handle errors when the initial connection is established
//works with on or once Mongoose method
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
//if connection error occured, output in the console
//console.error.bind(console) allows us to print errors in our terminal/browser console

db.once("open", () => console.log("We're connected to the cloud database"));

//Mongoose Schemas
//Schemas determine the structure of the documents to be written in the database
//Schemas act as blueprints to our data
//use the Schema() constructor of the Mongoose module to create anew Schema object
//the "new" keyword creates a brand new Schema as well
const taskSchema = new mongoose.Schema({
	//define fields with their corresponding data type

	name: String,
	status: {
		type: String,
		default : "pending"
	}
});

//Models
//uses schemas and are used to create/instantiate objects that correspond to the schema. must be single and start with a capital letter
//Server>Schema(blueprint)>Database>Collection

const Task = mongoose.model("Task", taskSchema)

//Setup for allowing the server to handle data from requests
app.use(express.json());
app.use(express.urlencoded({extended : true}));

//Create a new task
//Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		- if the task already exist in the database, we return an error
		- if the task does not exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task Object with a "name" field/property
	4. the "status" property does not need to be provided because our schema default it to "pending" upon creation of an object
*/

app.post("/tasks", (request, response) => {
	Task.findOne({name:request.body.name},(error,result) => {
		if(result != null && result.name == request.body.name){
			return response.send("Duplicate task found")
		}else{
			let newTask = new Task ({
				name : request.body.name
			});
			newTask.save((saveError,savedTask) =>{
				if(saveError){
					return console.error(saveError);
				} else{
					return response.status(201).send("New task created");
				}
			})
		}
	})
});

//Getting all the tasks
//Business logic
/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
app.get("/tasks", (request,response) =>{
	Task.find({}, (err,result) => {
		if(err){
			return console.error(err)
		} else {
			return response.status(200).json({
				data : result
			})
		}
	})
});

//Activity
 
const userSchema = new mongoose.Schema({
	username : String,
	password : String
});

const User = mongoose.model("User",userSchema);

app.post("/signup", (request, response) => {
	User.findOne({username:request.body.username},(error,result) => {
		if(result != null && result.username == request.body.username){
			return response.send("Duplicate user found")
		}else{
			let newUser = new User ({
				username : request.body.username,
				password : request.body.password
			});
			newUser.save((saveError,savedTask) =>{
				if(saveError){
					return console.error(saveError);
				} else{
					return response.status(201).send("New user registered");
				}
			})
		}
	})
});

app.get("/signup", (request,response) =>{
	User.find({}, (err,result) => {
		if(err){
			return console.error(err)
		} else {
			return response.status(200).json({
				data : result
			})
		}
	})
});

app.listen(port,() => console.log(`Server running at port ${port}.`))